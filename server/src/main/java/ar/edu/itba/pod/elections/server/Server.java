package ar.edu.itba.pod.elections.server;

import ar.edu.itba.pod.elections.services.ManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {
    private static Logger logger = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) throws RemoteException {
        logger.info("tpe-elections Server Starting ...");
        final ElectionServant es = new ElectionServant();
        final Remote remote = UnicastRemoteObject.exportObject(es, 0);


        final Registry registry = LocateRegistry.getRegistry();
        registry.rebind("managementService", remote);
        System.out.println("Management Service bound");

        registry.rebind("consultingService", remote);
        System.out.println("Consulting Service bound");

        registry.rebind("votingService", remote);
        System.out.println("Voting Service bound");

        registry.rebind("fiscalService", remote);
        System.out.println("Fiscal Service bound");
    }
}
