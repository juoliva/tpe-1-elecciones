package ar.edu.itba.pod.elections.server;

import ar.edu.itba.pod.elections.services.*;

import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Election {

    private ElectionState electionState;
    private static Election theElection;
    private final List<Vote> voteList;
    private HashMap<Integer, List<Fiscal>> registeredFiscals;
    private final Object fiscalsLock = "fiscalLock";
    private final Object electionStateLock = "electionStateLock";
    private final ExecutorService pool = Executors.newFixedThreadPool(15);
    private final ExecutorService resultsPool = Executors.newFixedThreadPool(3);

    private Future<STARresult> nationalResults;
    private Future<Map<ElectionRegion, SPAVresult>> regionalResults;

    public static Election getElection() {
        if (theElection == null) {
            theElection = new Election();
        }
        return theElection;
    }

    public ElectionState getElectionState(){
        return electionState;
    }

    public void openElection() throws IllegalStateException{
        synchronized (electionStateLock) {
            if (electionState==ElectionState.OPEN) {
                throw new IllegalStateException("Election is already Running");
            } else if (electionState==ElectionState.CLOSED){
                throw new IllegalStateException("Election is already Closed");
            }
            electionState = ElectionState.OPEN;
        }
    }

    private Election() {
        electionState=ElectionState.NOT_OPENED;
        voteList = Collections.synchronizedList(new ArrayList<>());
        registeredFiscals = new HashMap<>();
    }

    public void closeElection() throws IllegalStateException{
        synchronized (electionStateLock) {
            if (electionState == ElectionState.NOT_OPENED) {
                throw new IllegalStateException("Election is not Running");
            } else if (electionState == ElectionState.CLOSED) {
                throw new IllegalStateException("Election is already Closed");
            }
            electionState = ElectionState.CLOSED;
        }
            pool.shutdown();
            try {
                if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                    pool.shutdownNow();
                }
            } catch (InterruptedException ex) {
                pool.shutdownNow();
                Thread.currentThread().interrupt();
            }

        nationalResults = resultsPool.submit(this::computeNationalResults);
        regionalResults = resultsPool.submit(this::computeRegionalResults);

    }

    public void registerFiscal(Integer pollingPlace, ElectionParty party, NotifyVoteCallback notifyCallback) throws IllegalStateException{
        synchronized (electionStateLock) {
            if( electionState == ElectionState.CLOSED ) {
                throw new IllegalStateException("Election is already closed");
            }
        }

        synchronized (fiscalsLock) {
            if (registeredFiscals.get(pollingPlace) == null) {
                List<Fiscal> list = new ArrayList<>();
                list.add(new Fiscal(pollingPlace, party, notifyCallback));
                registeredFiscals.put(pollingPlace, list);
            } else {
                registeredFiscals.get(pollingPlace).add(new Fiscal(pollingPlace, party, notifyCallback));
            }
        }
    }

    private class Fiscal {
        private Integer pollingPlace;
        private ElectionParty party;
        private NotifyVoteCallback notifyCallback;

        public Fiscal(Integer pollingPlace, ElectionParty party, NotifyVoteCallback notifyCallback) {
            this.pollingPlace = pollingPlace;
            this.party = party;
            this.notifyCallback = notifyCallback;
        }

        public Integer getPollingPlace() {
            return pollingPlace;
        }

        public ElectionParty getParty() {
            return party;
        }

        public NotifyVoteCallback getNotifyCallback() {
            return notifyCallback;
        }

        void notifyVote() {
            try {
                notifyCallback.notifyVote();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    public List<Vote> getVoteList() {
        return voteList;
    }

    public void saveVote(Vote vote) throws IllegalStateException {
        synchronized (electionStateLock) {
            if( electionState != ElectionState.OPEN ) {
                throw new IllegalStateException("Election is not running");
            }
        }

        // Runnable votation = new Votation(vote);
        // Runnable notifyFiscal = new NotifyFiscal(vote);
        pool.submit( () -> voteList.add(vote)); // submit vote
        pool.submit(() -> { // notify fiscal
            synchronized ( fiscalsLock ) {
                if (registeredFiscals.get(vote.getPollingPlace()) != null) {
                    List<Fiscal> fiscalList = registeredFiscals.get(vote.getPollingPlace());
                    for (Fiscal fiscal : fiscalList) {
                        if (fiscal.getParty() == vote.getFptpVote()) {
                            fiscal.notifyVote();
                        }
                    }
                }
            }
        });
    }

    /*private class Votation implements Runnable
    {

        private Vote vote;

        public Votation(Vote vote) {
            this.vote = vote;
        }

        @Override
        public void run(){
            voteList.add(vote);
        }
    }

    private class NotifyFiscal implements Runnable
    {

        private Vote vote;

        public NotifyFiscal(Vote vote) {
            this.vote = vote;
        }

        @Override
        public void run(){

            synchronized ( fiscalsLock ) {
                if (registeredFiscals.get(vote.getPollingPlace()) != null) {
                    List<Fiscal> fiscalList = registeredFiscals.get(vote.getPollingPlace());
                    for (Fiscal fiscal : fiscalList) {
                        if (fiscal.getParty() == vote.getFptpVote()) {
                            fiscal.notifyVote();
                        }
                    }
                }
            }
        }
    }*/


    // CONSULTA DE VOTOS

    //Consulta de MESA
    public FPTPresult pollingPlaceResults(Integer pollingPlace) throws IllegalStateException {
        synchronized (electionStateLock) {
            if( electionState == ElectionState.NOT_OPENED ) {
                throw new IllegalStateException("Election has not started yet.");
            }
        }

        return getFPTPresults(pollingPlace,null, true);
    }
    //Consulta Regional
    public FPTPresult openRegionalResults( ElectionRegion region ) throws IllegalStateException {
        synchronized (electionStateLock) {
            if( electionState != ElectionState.OPEN ) {
                throw new IllegalStateException("Election is not open.");
            }
        }

        return getFPTPresults(null, region, true);
    }

    public FPTPresult openNationalResults() throws IllegalStateException {
        synchronized (electionStateLock) {
            if( electionState != ElectionState.OPEN ) {
                throw new IllegalStateException("Election is not open.");
            }
        }

        return getFPTPresults(null ,null, true);
    }

    public SPAVresult closedRegionalResults( ElectionRegion region ) throws IllegalStateException{
        synchronized (electionStateLock) {
            if( electionState != ElectionState.CLOSED ) {
                throw new IllegalStateException("Election is not closed.");
            }
        }

        Map<ElectionRegion, SPAVresult> result;

        try {
            result = regionalResults.get(300, TimeUnit.SECONDS);
        } catch (Exception e ) {
            resultsPool.shutdownNow();
            throw new IllegalStateException("Results could not be computed.");
        }

        return result.get(region);
    }

    public STARresult closedNationalResults() throws IllegalStateException {
        synchronized (electionStateLock) {
            if( electionState != ElectionState.CLOSED ) {
                throw new IllegalStateException("Election is not closed.");
            }
        }

        STARresult result;

        try {
            result = nationalResults.get(300, TimeUnit.SECONDS);
        } catch (Exception e ) {
            System.out.println(e.getMessage());
            resultsPool.shutdownNow();
            throw new IllegalStateException("Results could not be computed.");
        }

        return result;
    }

    private STARresult computeNationalResults() {
        HashMap<ElectionParty,Float> scoringRoundResults = new HashMap<>();
        for( Vote vote : voteList ) {
            for( ElectionParty ep : vote.getPartyScores().keySet()) {
                scoringRoundResults.put( ep , Optional.ofNullable( scoringRoundResults.get(ep) ).orElse((float)0) + vote.getPartyScores().get(ep) );
            }
        }
        ElectionParty winner;
        List<ElectionParty> finalists = new ArrayList<>();

        //Calculating finalists
        for (ElectionParty ep : scoringRoundResults.keySet()) {
            if (finalists.size() == 0) {
                finalists.add(ep);
            }
            else if(scoringRoundResults.get(ep).compareTo(scoringRoundResults.get(finalists.get(0))) > 0){
                finalists.set(0,ep);
            }
            else if(finalists.size() == 1){
                finalists.add(ep);
            }
            else if(scoringRoundResults.get(ep).compareTo(scoringRoundResults.get(finalists.get(1))) > 0){
                finalists.set(1,ep);
            }
            else if(scoringRoundResults.get(ep).compareTo(scoringRoundResults.get(finalists.get(0))) == 0){
                // Comparo alfabeticamente ep con finalist.get(0) y el menor alfabeticamente lo guardo
                if(finalists.get(0).name().compareTo(ep.name()) > 0){
                    finalists.add(0,ep);
                }
            }
            else if(scoringRoundResults.get(ep).compareTo(scoringRoundResults.get(finalists.get(1))) == 0){
                // Comparo alfabeticamente ep con finalist.get(1) y el menor alfabeticamente lo guardo
                if(finalists.get(1).name().compareTo(ep.name()) > 0){
                    finalists.add(1,ep);
                }
            }
        }

        //Automatic Runoff
        HashMap<ElectionParty,Float> aux = new HashMap<>();
        aux.put(finalists.get(0),(float)0);
        aux.put(finalists.get(1),(float)0);

        Integer a = 0,b = 0;
        for (Vote v : getVoteList()) {
                a = Optional.ofNullable(v.getPartyScores().get(finalists.get(0))).orElse(0);
                b = Optional.ofNullable(v.getPartyScores().get(finalists.get(1))).orElse(0);
            if(!(a==0 && b==0)){
                if(a.equals(b)){
                    if(finalists.get(0).name().compareTo(finalists.get(1).name()) <= 0){
                        aux.put(finalists.get(0),aux.get(finalists.get(0))+1);
                    }
                    else{
                        aux.put(finalists.get(1),aux.get(finalists.get(1))+1);
                    }
                }
                else{
                    if(a > b){
                        aux.put(finalists.get(0),aux.get(finalists.get(0))+1);
                    }
                    else{
                        aux.put(finalists.get(1),aux.get(finalists.get(1))+1);
                    }
                }
            }
        }

        if(aux.get(finalists.get(0)) > aux.get(finalists.get(1))){
            winner = finalists.get(0);
        }
        else if(aux.get(finalists.get(0)) < aux.get(finalists.get(1))){
            winner = finalists.get(1);
        }
        else {
            if(finalists.get(0).name().compareTo(finalists.get(1).name()) <= 0){
                winner=finalists.get(0);
            }
            else{
                winner=finalists.get(1);
            }
        }

        float total = aux.get(finalists.get(0)) + aux.get(finalists.get(1));

        aux.replaceAll((e, v) -> aux.get(e) * ( (float) 100 / total));

        return new STARresult(scoringRoundResults, aux, winner);
    }

    private Map<ElectionRegion, SPAVresult> computeRegionalResults() {
        Map<ElectionRegion, SPAVresult> results = new HashMap<>();

        for ( ElectionRegion er : ElectionRegion.values() ) {
            List<Vote> regionVotes = voteList.stream().filter( vote -> vote.getRegion().equals(er) ).collect(Collectors.toList());
            HashMap<ElectionParty, Float> firstRoundResults;
            HashMap<ElectionParty, Float> secondRoundResults = null;
            HashMap<ElectionParty, Float> thirdRoundResults = null;

            Integer m = 0;
            ArrayList<ElectionParty> winners = new ArrayList<>();
            HashMap<ElectionParty, Float> selected = new HashMap<>();

            // Primer ronda
            for (Vote vote : regionVotes) {
                    for (ElectionParty ep : vote.getPartyScores().keySet()) {
                        selected.put(ep, Optional.ofNullable(selected.get(ep)).orElse((float)0) + 1);
                    }
            }
            firstRoundResults = selected;

            // elijo winner first round
            for (ElectionParty ep : selected.keySet()) {
                if (winners.size() == 0) {
                    winners.add(ep);
                } else if (selected.get(winners.get(0)) < selected.get(ep)) {
                    winners.set(0, ep);
                } else if (selected.get(winners.get(0)).equals(selected.get(ep))) {
                    if (winners.get(0).name().compareTo(ep.name()) > 0) {
                        winners.set(0, ep);
                    }
                }
            }

            // Segunda ronda
            secondRoundResults = computeMAndVotes(winners, regionVotes);
            // Elijo winner second round
            for (ElectionParty ep : secondRoundResults.keySet()) {
                if (winners.size() == 1) {
                    winners.add(ep);
                } else if (secondRoundResults.get(winners.get(1)) < secondRoundResults.get(ep)) {
                    winners.set(1, ep);
                } else if (secondRoundResults.get(winners.get(1)).equals(secondRoundResults.get(ep))) {
                    if (winners.get(1).name().compareTo(ep.name()) > 0) {
                        winners.set(1, ep);
                    }
                }
            }

            // Tercera ronda
            thirdRoundResults = computeMAndVotes(winners, regionVotes);

            // Elijo winner third round
            for (ElectionParty ep : thirdRoundResults.keySet()) {
                if (winners.size() == 2) {
                    winners.add(ep);
                } else if (thirdRoundResults.get(winners.get(2)) < thirdRoundResults.get(ep)) {
                    winners.set(2, ep);
                } else if (thirdRoundResults.get(winners.get(2)).equals(thirdRoundResults.get(ep))) {
                    if (winners.get(2).name().compareTo(ep.name()) > 0) {
                        winners.set(2, ep);
                    }
                }
            }

            results.put(er, new SPAVresult(winners, firstRoundResults, secondRoundResults, thirdRoundResults));
        }

        return results;
    }

    private HashMap<ElectionParty, Float> computeMAndVotes(ArrayList<ElectionParty> winners, List<Vote> regionVotes) {
        HashMap<ElectionParty, Float> roundResults;
        int m;
        roundResults = new HashMap<>();
        for (Vote vote : regionVotes) {
            m = 0;
            for (ElectionParty ep : vote.getPartyScores().keySet()) {
                if (winners.contains(ep)) {
                    m++;
                }
            }
            for (ElectionParty ep : vote.getPartyScores().keySet()) {
                roundResults.put(ep, Optional.ofNullable(roundResults.get(ep)).orElse((float)0) + ( (float) 1 / (1 + m)));
            }
        }

        for( ElectionParty ep : winners ) {
            roundResults.remove(ep);
        }
        return roundResults;
    }

    public FPTPresult getFPTPresults(Integer pollingPlace, ElectionRegion region, Boolean percentage){
        int totalVotes = 0;
        HashMap<ElectionParty,Float> rta = new HashMap<>();

        if(pollingPlace != null){
            synchronized (voteList){
                for (Vote v : getVoteList()) {
                    if (v.getPollingPlace().equals(pollingPlace)) {
                        totalVotes++;
                        if (rta.get(v.getFptpVote()) == null) {
                            rta.put(v.getFptpVote(), (float) 1);
                        } else {
                            rta.put(v.getFptpVote(), rta.get(v.getFptpVote()) + 1);
                        }
                    }
                }
            }
        }
        else if(region != null){
            synchronized (voteList) {
                for (Vote v : voteList) {
                    if (v.getRegion() == region) {
                        totalVotes++;
                        if (rta.get(v.getFptpVote()) == null) {
                            rta.put(v.getFptpVote(), (float) 1);
                        } else {
                            rta.put(v.getFptpVote(), rta.get(v.getFptpVote()) + 1);
                        }
                    }
                }
            }
        }
        else{
            synchronized (voteList) {
                for (Vote v : voteList) {
                    totalVotes++;
                    if (rta.get(v.getFptpVote()) == null) {
                        rta.put(v.getFptpVote(), (float) 1);
                    } else {
                        rta.put(v.getFptpVote(), rta.get(v.getFptpVote()) + 1);
                    }
                }
            }
        }
        if(percentage){
            //Pasamos a porcentajes
            if (totalVotes != 0){
                for (ElectionParty ep : rta.keySet()) {
                    Float res = rta.get(ep) * ( (float) 100 / totalVotes);
                    rta.put(ep,res );
                }
            }
        }


        return new FPTPresult(rta);
    }
}
