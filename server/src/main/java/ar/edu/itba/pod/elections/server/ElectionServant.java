package ar.edu.itba.pod.elections.server;
// Esta clase lo que va a hacer es entregar al cliente
// los Servicios de : {Administracion, Votacion, Fiscalizacion, Consulta }



import ar.edu.itba.pod.elections.services.*;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ElectionServant implements ManagementService, VotingService, FiscalService, ConsultingService {
    private Election election;
    public ElectionServant() throws RemoteException {
       election = Election.getElection();
    }

    // ManagementService

    @Override
    public void openElection() throws IllegalStateException{
        election.openElection();
        //Imprimir en pantalla el estado de los comicios luego de invocar a la acción o el error correspondiente.
        // Check Thread Safe
    }

    @Override
    public ElectionState stateElection() {
        return election.getElectionState();
        // Imprimir en pantalla el estado de los comicios al momento de la consulta.
    }

    @Override
    public void closeElection() throws IllegalStateException {
        election.closeElection();
        // Imprimir en pantalla el estado de los comicios luego de invocar a la acción o el error correspondiente.
        // Check Thread safety

    }

    // VotingService

    @Override
    public void emitVote(Vote vote) throws IllegalStateException {

        election.saveVote(vote);
    }

    // FiscalService

    @Override
    public void registerFiscal(Integer pollingPlace, ElectionParty party, NotifyVoteCallback notifyCallback) throws IllegalStateException{
        election.registerFiscal(pollingPlace, party, notifyCallback);
    }

    // ConsultingService


    @Override
    public STARresult closedNationalResults() throws IllegalStateException {
        return election.closedNationalResults();
    }

    @Override
    public FPTPresult openNationalResults() throws IllegalStateException {
        return election.openNationalResults();
    }

    @Override
    public FPTPresult openRegionalResults( ElectionRegion region ) throws IllegalStateException {
        return election.openRegionalResults(region);

    }
    @Override
    public SPAVresult closedRegionalResults( ElectionRegion region ) throws IllegalStateException  {
        return election.closedRegionalResults(region);
    }
    @Override
    public FPTPresult pollingPlaceResults( Integer pollingPlace ) throws IllegalStateException {
        return election.pollingPlaceResults(pollingPlace);
    }


}

//// DECISION
// Esta clase va a ser un singleton: para que en cada servidor haya una unica eleccion. Esto lo decidimos
// por el siguiente requisit "Se puede considerar que el servicio funciona por elección. Es decir, se prende un servidor para una elección y al final luego de consultados los resultados, se lo apaga sin persistir la información."