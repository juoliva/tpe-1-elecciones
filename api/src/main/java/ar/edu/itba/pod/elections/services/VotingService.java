package ar.edu.itba.pod.elections.services;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface VotingService extends Remote {

    void emitVote(Vote vote) throws RemoteException, IllegalStateException ;

}
