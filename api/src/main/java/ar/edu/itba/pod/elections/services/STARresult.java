package ar.edu.itba.pod.elections.services;

import java.io.Serializable;
import java.util.HashMap;

public class STARresult implements Serializable {
    HashMap<ElectionParty, Float> scoringRoundResults;
    HashMap<ElectionParty, Float> finalResults;
    ElectionParty winner;

    public STARresult(HashMap<ElectionParty, Float> scoringRoundResults, HashMap<ElectionParty, Float> finalResults, ElectionParty winner) {
        this.scoringRoundResults = scoringRoundResults;
        this.finalResults = finalResults;
        this.winner = winner;
    }

    public HashMap<ElectionParty, Float> getScoringRoundResults() {
        return scoringRoundResults;
    }

    public HashMap<ElectionParty, Float> getFinalResults() {
        return finalResults;
    }

    public ElectionParty getWinner() {
        return winner;
    }

    @Override
    public String toString() {
        return "STARresult{" +
                "scoringRoundResults=" + scoringRoundResults +
                ", finalResults=" + finalResults +
                ", winner=" + winner +
                '}';
    }
}
