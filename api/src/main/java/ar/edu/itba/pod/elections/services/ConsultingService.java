package ar.edu.itba.pod.elections.services;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;

public interface ConsultingService extends Remote {

    // devuelven los resultados FPTP.

    STARresult closedNationalResults() throws RemoteException, IllegalStateException;

    FPTPresult openNationalResults() throws RemoteException, IllegalStateException;

    FPTPresult openRegionalResults( ElectionRegion region ) throws RemoteException, IllegalStateException;

    FPTPresult pollingPlaceResults( Integer pollingPlace ) throws RemoteException, IllegalStateException;

    SPAVresult closedRegionalResults( ElectionRegion region ) throws RemoteException, IllegalStateException;
}