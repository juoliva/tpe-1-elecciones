package ar.edu.itba.pod.elections.services;

import java.io.Serializable;
import java.util.HashMap;

public class FPTPresult implements Serializable {
    HashMap<ElectionParty,Float> results;

    public FPTPresult(HashMap<ElectionParty, Float> results) {
        this.results = results;
    }

    public HashMap<ElectionParty, Float> getResults() {
        return results;
    }

    public ElectionParty getWinner(){
        if(results == null){
            return null;
        }
        ElectionParty winner = null;
        for (ElectionParty ep: results.keySet()) {
            if (winner == null){
                winner = ep;
            }
            else if( results.get(ep) > results.get(winner)){
                winner = ep;
            }
        }
        return winner;
    }


    @Override
    public String toString() {
        return "FPTPresult{" +
                "results=" + results +
                '}';
    }
}
