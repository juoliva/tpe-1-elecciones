package ar.edu.itba.pod.elections.services;

public enum ElectionParty {
    TIGER,
    LEOPARD,
    LYNX,
    TURTLE,
    OWL,
    JACKALOPE,
    BUFFALO
}
