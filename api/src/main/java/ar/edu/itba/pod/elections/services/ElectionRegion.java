package ar.edu.itba.pod.elections.services;

public enum ElectionRegion {
    JUNGLE,
    SAVANNAH,
    TUNDRA
}
