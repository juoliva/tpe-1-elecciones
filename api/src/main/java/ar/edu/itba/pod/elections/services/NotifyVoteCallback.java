package ar.edu.itba.pod.elections.services;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NotifyVoteCallback  extends Remote{
    void notifyVote() throws RemoteException;
}
