package ar.edu.itba.pod.elections.services;

import java.rmi.Remote;
import java.rmi.RemoteException;


//● Funcionalidad: Apertura y cierre de los comicios.
//        ● Usuario: Autoridades gubernamentales
//        ● Debe contar con métodos para:
//        ○ Abrir los comicios. Si la elección ya finalizó, debe arrojar un error.
//        4
//        72.42 Programación de Objetos Distribuidos
//        ○ Consultar el estado de los comicios: indicando si están sin iniciar, iniciados o finalizados.
//        ○ Finalizar los comicios. Si las elecciones no se iniciaron, debe arrojar un error.

public interface ManagementService extends Remote {

    void openElection() throws RemoteException;
    ElectionState stateElection() throws RemoteException;
    void closeElection() throws RemoteException;

}
