package ar.edu.itba.pod.elections.services;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class SPAVresult implements Serializable {
    List<ElectionParty> winners;
    HashMap<ElectionParty, Float> firstRoundResults;
    HashMap<ElectionParty, Float> secondRoundResults;
    HashMap<ElectionParty, Float> thirdRoundResults;

    public SPAVresult(List<ElectionParty> winners, HashMap<ElectionParty, Float> firstRoundResults, HashMap<ElectionParty, Float> secondRoundResults, HashMap<ElectionParty, Float> thirdRoundResults) {
        this.winners = winners;
        this.firstRoundResults = firstRoundResults;
        this.secondRoundResults = secondRoundResults;
        this.thirdRoundResults = thirdRoundResults;
    }

    public List<ElectionParty> getWinners() {
        return winners;
    }

    public HashMap<ElectionParty, Float> getFirstRoundResults() {
        return firstRoundResults;
    }

    public HashMap<ElectionParty, Float> getSecondRoundResults() {
        return secondRoundResults;
    }

    public HashMap<ElectionParty, Float> getThirdRoundResults() {
        return thirdRoundResults;
    }

    @Override
    public String toString() {
        return "SPAVresult{" +
                "winners=" + winners +
                ", firstRoundResults=" + firstRoundResults +
                ", secondRoundResults=" + secondRoundResults +
                ", thirdRoundResults=" + thirdRoundResults +
                '}';
    }
}
