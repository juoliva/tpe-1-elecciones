package ar.edu.itba.pod.elections.services;

public enum ElectionState {
    NOT_OPENED,
    OPEN,
    CLOSED
}
