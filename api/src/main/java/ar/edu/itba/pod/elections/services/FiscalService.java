package ar.edu.itba.pod.elections.services;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FiscalService extends Remote {

    void registerFiscal(Integer pollingPlace, ElectionParty party, NotifyVoteCallback notifyCallback) throws RemoteException, IllegalStateException;
}