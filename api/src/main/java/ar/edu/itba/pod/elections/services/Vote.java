package ar.edu.itba.pod.elections.services;

import java.io.Serializable;
import java.util.HashMap;

public class Vote implements Serializable {
    private Integer pollingPlace;
    private ElectionRegion region;
    private HashMap<ElectionParty, Integer> partyScores;
    private ElectionParty fptpVote;

    public Vote(Integer pollingPlace, ElectionRegion region, HashMap<ElectionParty, Integer> partyScores, ElectionParty fptpVote) {
        this.pollingPlace = pollingPlace;
        this.region = region;
        this.partyScores = partyScores;
        this.fptpVote = fptpVote;
    }

    public Integer getPollingPlace() {
        return pollingPlace;
    }

    public ElectionRegion getRegion() {
        return region;
    }

    public HashMap<ElectionParty, Integer> getPartyScores() {
        return partyScores;
    }

    public ElectionParty getFptpVote() {
        return fptpVote;
    }
}
