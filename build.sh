#!/bin/bash

mvn install;

tar -xzf server/target/tpe-elections-server-1.0-SNAPSHOT-bin.tar.gz
tar -xzf client/target/tpe-elections-client-1.0-SNAPSHOT-bin.tar.gz

chmod u+x tpe-elections-client-1.0-SNAPSHOT/run-*
chmod u+x tpe-elections-server-1.0-SNAPSHOT/run-*