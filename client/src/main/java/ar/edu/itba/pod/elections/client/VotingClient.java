package ar.edu.itba.pod.elections.client;

import ar.edu.itba.pod.elections.services.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.HashMap;

public class VotingClient {

    public static void main(String[] args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        Options options = new Options();

        Option ip = new Option("DserverAddress", "DserverAddress", true, "Server IP address");
        ip.setRequired(true);
        options.addOption(ip);

        Option path = new Option("DvotesPath", "DvotesPath", true, "Path file of votes");
        path.setRequired(true);
        options.addOption(path);

        cmd = parser.parse(options, args);

        String serverAddress = cmd.getOptionValue("DserverAddress");
        VotingService votingService = (VotingService) Naming.lookup("//" + serverAddress + "/votingService"); //  //127.0.0.1:1099/votingService

        String voteFile = cmd.getOptionValue("DvotesPath");

        CSVParser csvParser;
        try {
            int votCount = 0;
            Reader reader = Files.newBufferedReader(Paths.get(voteFile));
            csvParser = new CSVParser(reader, CSVFormat.newFormat(';'));
            for (CSVRecord csvRecord : csvParser) {
                HashMap<ElectionParty, Integer> partyScores = new HashMap<>();
                Integer pollingPlace = Integer.valueOf(csvRecord.get(0));
                ElectionRegion region = ElectionRegion.valueOf(csvRecord.get(1));
                String[] points = csvRecord.get(2).split(",");

                for ( String point : points ) {
                    partyScores.put(ElectionParty.valueOf(point.split("\\|")[0]),Integer.valueOf(point.split("\\|")[1]));
                }

                ElectionParty fptpVote = ElectionParty.valueOf(csvRecord.get(3));


                try {
                    votingService.emitVote(new Vote(pollingPlace, region, partyScores, fptpVote));
                    votCount++;
                } catch (IllegalStateException e) { System.out.println(e.getMessage());
                                                    break;}

            }
            System.out.println(votCount+ " votes Registered");
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }




    }
}