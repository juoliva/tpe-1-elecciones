package ar.edu.itba.pod.elections.client;

import ar.edu.itba.pod.elections.services.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class ConsultingClient {

    public static void main(String[] args) throws ParseException, RemoteException, NotBoundException, MalformedURLException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        Options options = new Options();

        Option ip = new Option("DserverAddress", "DserverAddress", true, "Server IP address");
        ip.setRequired(true);
        options.addOption(ip);

        Option pollingPlace = new Option("Did", "Did", true, "Table ID");
        pollingPlace.setRequired(false);
        options.addOption(pollingPlace);

        Option province = new Option("Dstate", "Dstate", true, "Province name");
        province.setRequired(false);
        options.addOption(province);

        Option path = new Option("DoutPath", "DoutPath", true, "Elections results paths");
        path.setRequired(true);
        options.addOption(path);

        cmd = parser.parse(options, args);

        String pollingPlaceS = cmd.getOptionValue("Did");
        String provinceS = cmd.getOptionValue("Dstate");
        String csvFilePath = cmd.getOptionValue("DoutPath");
        String serverAddress = cmd.getOptionValue("DserverAddress");


        ManagementService managementService = (ManagementService) Naming.lookup("//" + serverAddress + "/managementService"); //  //127.0.0.1:1099/votingService
        ConsultingService consultingService = (ConsultingService) Naming.lookup("//" + serverAddress + "/consultingService"); //  //127.0.0.1:1099/votingService

        ElectionState es = managementService.stateElection();
        if (es == ElectionState.NOT_OPENED) {
            System.out.println("ELections are not opened yet");
            return;

        }

        // ERROR DE ARGUMENTOS
        if (pollingPlaceS != null && provinceS != null) {
            System.out.println("Pick table or province.");
            return;
        }


//  QUIERO RESULTADO NACIONAL
        else if (pollingPlaceS == null && provinceS == null) {
            if (es == ElectionState.OPEN) {
                try {
                    FPTPresult fptPresult = consultingService.openNationalResults();
                    writePercentageParty(csvFilePath, fptPresult.getResults());
                } catch ( IllegalStateException e ) {
                    e.getMessage();
                }
            } else if (es == ElectionState.CLOSED) {
                try {
                    STARresult starResult = consultingService.closedNationalResults();
                    System.out.println(starResult);
                    writeScoreParty(csvFilePath, starResult.getScoringRoundResults());
                    writePercentageParty(csvFilePath, starResult.getFinalResults());
                    ArrayList<ElectionParty> e = new ArrayList<>();
                    e.add(starResult.getWinner());
                    writeWinner(csvFilePath, e);
                } catch ( IllegalStateException e ) {
                    e.getMessage();
                }
            }
        }

//  QUIERO RESULTADO PROVINCIAL
        else if (provinceS != null) {
            if (es == ElectionState.OPEN) {
                try {
                    FPTPresult fptPresult = consultingService.openRegionalResults(ElectionRegion.valueOf(provinceS));
                    writePercentageParty(csvFilePath, fptPresult.getResults());
                } catch ( IllegalStateException e ) {
                    e.getMessage();
                }

            } else if (es == ElectionState.CLOSED) {
                try {
                    SPAVresult spaVresult = consultingService.closedRegionalResults(ElectionRegion.valueOf(provinceS));
                    writeRound(csvFilePath, 1);
                    writeApprovalParty(csvFilePath, spaVresult.getFirstRoundResults());
                    writeWinner(csvFilePath, spaVresult.getWinners().subList(0, 1));
                    writeRound(csvFilePath, 2);
                    writeApprovalParty(csvFilePath, spaVresult.getSecondRoundResults());
                    writeWinner(csvFilePath, spaVresult.getWinners().subList(0, 2));
                    writeRound(csvFilePath, 3);
                    writeApprovalParty(csvFilePath, spaVresult.getThirdRoundResults());
                    writeWinner(csvFilePath, spaVresult.getWinners().subList(0, 3));
                } catch ( IllegalStateException e ) {
                    e.getMessage();
                }
            }
        }
//  QUIERO RESULTADO MESAL
        else if (pollingPlaceS != null) {
            try {
                FPTPresult fptPresult = consultingService.pollingPlaceResults(Integer.parseInt(pollingPlaceS));
                writePercentageParty(csvFilePath, fptPresult.getResults());
                ArrayList<ElectionParty> e = new ArrayList<>();
                e.add(fptPresult.getWinner());
                writeWinner(csvFilePath, e);
            } catch ( IllegalStateException e ) {
                e.getMessage();
            }

        }
    }

    public static void writePercentageParty(String file, Map<ElectionParty, Float> results) {
        try {
            FileWriter pw = new FileWriter(file, true);
            final CSVPrinter csvPrinter = new CSVPrinter(pw, CSVFormat.newFormat(';').withHeader("Percentage", "Party").withRecordSeparator('\n'));
            results.entrySet().stream().sorted((o1, o2) -> {
                if (!o1.getValue().equals(o2.getValue())) return Float.compare(o2.getValue(), o1.getValue());
                else return o1.getKey().compareTo(o2.getKey());
            }).forEach(entry -> {
                ElectionParty party = entry.getKey();
                DecimalFormat format = new DecimalFormat("##.00", new DecimalFormatSymbols(Locale.ENGLISH));
                String percent = format.format(entry.getValue()) + "%";
                try {
                    csvPrinter.printRecord(percent, party);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            csvPrinter.flush();

        } catch (IOException e) {
            System.out.println("Error while printing CSV file.");
        }

    }

    public static void writeScoreParty(String file, Map<ElectionParty, Float> results) {
        try {
            FileWriter pw = new FileWriter(file, true);
            final CSVPrinter csvPrinter = new CSVPrinter(pw, CSVFormat.newFormat(';').withHeader("Score", "Party").withRecordSeparator('\n'));
            results.entrySet().stream().sorted((o1, o2) -> {
                if (!o1.getValue().equals(o2.getValue())) return Double.compare(o2.getValue(), o1.getValue());
                else return o1.getKey().compareTo(o2.getKey());
            }).forEach(entry -> {
                ElectionParty party = entry.getKey();
                DecimalFormat format = new DecimalFormat();
                String percent = format.format(entry.getValue());
                try {
                    csvPrinter.printRecord(percent, party);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            csvPrinter.flush();
        } catch (IOException e) {
            System.out.println("Error while printing CSV file.");
        }
    }

    public static void writeApprovalParty(String file, Map<ElectionParty, Float> results) {
        try {
            FileWriter pw = new FileWriter(file, true);
            final CSVPrinter csvPrinter = new CSVPrinter(pw, CSVFormat.newFormat(';').withHeader("Approval", "Party").withRecordSeparator('\n'));
            results.entrySet().stream().sorted((o1, o2) -> {
                if (!o1.getValue().equals(o2.getValue())) return Double.compare(o2.getValue(), o1.getValue());
                else return o1.getKey().compareTo(o2.getKey());
            }).forEach(entry -> {
                ElectionParty party = entry.getKey();
                DecimalFormat format = new DecimalFormat("##.00");
                String percent = format.format(entry.getValue());
                try {
                    csvPrinter.printRecord(percent, party);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            csvPrinter.flush();
        } catch (IOException e) {
            System.out.println("Error while printing CSV file.");
        }
    }

    public static void writeWinner(String file, List<ElectionParty> ep) {
        try {
            FileWriter pw = new FileWriter(file, true);
            final CSVPrinter csvPrinter = new CSVPrinter(pw, CSVFormat.newFormat(';').withHeader("Winner \n").withRecordSeparator(" "));
            for (ElectionParty epp : ep) {
                try {
                    csvPrinter.printRecord(epp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            csvPrinter.flush();
        } catch (IOException e) {
            System.out.println("Error while printing CSV file.");
        }
    }

    public static void writeRound(String file, Integer round) {
        try {
            FileWriter pw = new FileWriter(file, true);
            final CSVPrinter csvPrinter = new CSVPrinter(pw, CSVFormat.newFormat(';').withHeader("Round " + round).withRecordSeparator('\n'));
            csvPrinter.flush();
        } catch (IOException e) {
            System.out.println("Error while printing CSV file.");
        }
    }

}
//        Map<ElectionParty, Float> results = new HashMap<>();
//        ElectionParty e1 = ElectionParty.JACKALOPE;
//        ElectionParty e2 = ElectionParty.TURTLE;
//        ElectionParty e3 = ElectionParty.LYNX;
//        results.put(e1,(float)25);
//        results.put(e2,(float)15);
//        results.put(e3,(float)45);
//
//        List<ElectionParty> arrep = new ArrayList<>();
//        arrep.add(e1);
//        arrep.add(e2);
//        arrep.add(e3);
//        writeScoreParty("/Users/darkovindis/Desktop/FACULTAD/CURSANDO/POD/test.csv", results);
//        writeWinner("/Users/darkovindis/Desktop/FACULTAD/CURSANDO/POD/test.csv", arrep);
