package ar.edu.itba.pod.elections.client;

import ar.edu.itba.pod.elections.services.ElectionParty;
import ar.edu.itba.pod.elections.services.ElectionRegion;
import ar.edu.itba.pod.elections.services.NotifyVoteCallback;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class NotifyVoteCallbackImpl extends UnicastRemoteObject implements NotifyVoteCallback{
    ElectionParty party;
    Integer pollingPlace;

    public NotifyVoteCallbackImpl(ElectionParty party, Integer pollingPlace) throws RemoteException {
        this.party = party;
        this.pollingPlace = pollingPlace;
    }

    @Override
    public void notifyVote() {
        System.out.println("New vote for "+party+" on polling place " + pollingPlace);
    }
}
