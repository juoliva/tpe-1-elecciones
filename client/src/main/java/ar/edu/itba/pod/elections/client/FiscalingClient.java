package ar.edu.itba.pod.elections.client;

import ar.edu.itba.pod.elections.services.ElectionParty;
import ar.edu.itba.pod.elections.services.FiscalService;
import ar.edu.itba.pod.elections.services.ManagementService;
import ar.edu.itba.pod.elections.services.NotifyVoteCallback;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class FiscalingClient {

    public static void main(String[] args) throws ParseException, RemoteException, NotBoundException, MalformedURLException {

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        Options options = new Options();

        Option ip = new Option("DserverAddress", "DserverAddress", true, "Server IP address");
        ip.setRequired(true);
        options.addOption(ip);

        Option table = new Option("Did", "Did", true, "Table id");
        table.setRequired(true);
        options.addOption(table);

        Option party = new Option("Dparty", "Dparty", true, "Party name");
        party.setRequired(true);
        options.addOption(party);

        cmd = parser.parse(options, args);

        String pollingPlace = cmd.getOptionValue("Did");
        String partyS = cmd.getOptionValue("Dparty");
        String serverAddress = cmd.getOptionValue("DserverAddress");

        FiscalService fiscalService = (FiscalService) Naming.lookup("//" + serverAddress + "/fiscalService"); //  //127.0.0.1:1099/votingService

        try {
            fiscalService.registerFiscal(Integer.parseInt(pollingPlace), ElectionParty.valueOf(partyS), new NotifyVoteCallbackImpl(ElectionParty.valueOf(partyS),Integer.parseInt(pollingPlace)));
            System.out.println("Fiscal of "+ partyS +" registered on polling place "+ pollingPlace + "\n");
        } catch ( IllegalStateException e ) {
            System.out.println(e.getMessage());
        }
    }
}