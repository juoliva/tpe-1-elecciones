package ar.edu.itba.pod.elections.client;

import ar.edu.itba.pod.elections.services.ManagementService;
import ar.edu.itba.pod.elections.services.VotingService;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.Naming;
import java.rmi.RemoteException;

public class ManagementClient {
    private static ManagementService managementService;

    public static void main(String[] args) throws Exception  {
        managementService = (ManagementService) Naming.lookup("//127.0.0.1:1099/managementService");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        Options options = new Options();

        Option ip = new Option("DserverAddress", "DserverAddress", true, "Server IP address");
        ip.setRequired(true);
        options.addOption(ip);

        Option action = new Option("Daction", "Daction", true, "Action of OPEN or CLOSE or GET_STATE of election");
        action.setRequired(true);
        options.addOption(action);

        cmd = parser.parse(options, args);

        String serverAddress = cmd.getOptionValue("DserverAddress");
        String actionS = cmd.getOptionValue("Daction");

        switch (actionS){
            case "open":
                openElection();
                break;
            case "close":
                closeElection();
                break;
            case "state":
                getElectionState();
                break;
        }
    }


    public static void getElectionState() throws RemoteException {
        switch (managementService.stateElection()){
            case OPEN: System.out.println("Election Running");
                break;
            case NOT_OPENED: System.out.println("Election Not Running");
                break;
            case CLOSED: System.out.println("Election Closed");
                break;

        }
    }

    public static void openElection() throws RemoteException {
        try {
            managementService.openElection();
            System.out.println("Election Started");
        } catch (IllegalStateException e) { System.out.println(e.getMessage());}
    }

    public static void closeElection() throws RemoteException {
        try {
            managementService.closeElection();
            System.out.println("Election Closed");
        } catch (IllegalStateException e) { System.out.println(e.getMessage());}
    }

}
