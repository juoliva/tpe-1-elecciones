package ar.edu.itba.pod.elections.client;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;

public class CSV {

    static void readVotesFromCSV2(final String path) throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader(path)))
        {
            String l;
            for(int i = 0; (l = br.readLine()) != null; i++)
            {
                String[] vote = l.split(";");
                try {
                    // integration: vote[0] = mesa, vote[1] = provincia, vote[2].split(",") = elecciones

                } catch(IllegalArgumentException e) {
                    System.out.println("Error while reading CSV file.");
                }
            }
        }
    }

    public static void readVotesFromCSV(String file) throws RemoteException {
        CSVParser csvParser = null;
        try {
            Reader reader = Files.newBufferedReader(Paths.get(file));
            csvParser = new CSVParser(reader, CSVFormat.newFormat(';'));
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }
        for (CSVRecord csvRecord : csvParser) {
            String table = csvRecord.get(0);
            String province = csvRecord.get(1);
            String[] choices = csvRecord.get(2).split(",");

            // integration: table = mesa, province = provincia, choices = elecciones from CSV
        }
    }

    public static void writeCSV(String file, Map<String, Double> results) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(file));
            final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.newFormat(';').withHeader("Porcentaje", "Partido").withRecordSeparator('\n'));
            results.entrySet().stream().sorted((o1, o2) -> {
                if(!o1.getValue().equals(o2.getValue())) return Double.compare(o2.getValue(),o1.getValue());
                else return o1.getKey().compareTo(o2.getKey());
            }).forEach(entry -> {
                String party = entry.getKey();
                DecimalFormat format = new DecimalFormat("##.00");
                String percent = format.format(entry.getValue() * 100) + "%";
                try {
                    csvPrinter.printRecord(percent, party);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            csvPrinter.flush();
        } catch (IOException e){
            System.out.println("Error while printing CSV file.");
        }
    }
}